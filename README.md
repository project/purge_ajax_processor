CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * MODULE FEATURES
 * Configuration
 * Maintainers


INTRODUCTION
------------
Provides a Purge processor which Sends an Ajax request on every page load to
process the purge queue.

REQUIREMENTS
------------

Purge module. See: https://www.drupal.org/project/purge.


INSTALLATION
------------

Refer the Purge documentation to get more details on how to setup the various
purge modules and processors.
https://git.drupalcode.org/project/purge/-/blob/8.x-3.x/README.md

MODULE FEATURES
---------------
 * Provides a purge processor named Ajax processor which processes the purge
   queue using ajax requests.

 * The ajax requests are fired via JS at a special dedicated path `/purge-ajax-processor`

 * For anonymous users the purge processor endpoint is cached.

 * While adding the Ajax processor admins can control for how long the endpoint
   will be cached.

 * Since purge request is fired via ajax it provides and instant content refresh mechanism.

CONFIGURATION
-------------

 * Refer https://git.drupalcode.org/project/purge/-/blob/8.x-3.x/README.md for
   setting up purge processors.

 * After adding the processor click on the configure button of processor to
   configure the Maximum age for the endpoint.

MAINTAINERS
-----------

Current maintainers:
 * Hemant Gupta (https://www.drupal.org/u/guptahemant)


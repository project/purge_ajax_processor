<?php

namespace Drupal\purge_ajax_processor\Plugin\Purge\Processor;

use Drupal\purge\Plugin\Purge\Processor\ProcessorBase;
use Drupal\purge\Plugin\Purge\Processor\ProcessorInterface;

/**
 * Late runtime processor.
 *
 * @PurgeProcessor(
 *   id = "ajax_processor",
 *   label = @Translation("Ajax processor"),
 *   description = @Translation("Process the purge queue on every page load via ajax."),
 *   enable_by_default = true,
 *   configform = "Drupal\purge_ajax_processor\Form\ConfigurationForm",
 * )
 */
class AjaxProcessor extends ProcessorBase implements ProcessorInterface {

}

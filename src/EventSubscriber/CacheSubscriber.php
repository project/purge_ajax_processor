<?php

namespace Drupal\purge_ajax_processor\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\purge_ajax_processor\Form\ConfigurationForm;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * A subscriber for setting the external max age of purge ajax processor path.
 */
class CacheSubscriber implements EventSubscriberInterface {

  /**
   * A config object for the purge_ajax_processor config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $processorConfig;

  /**
   * Constructor for CacheSubscriber.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->processorConfig = $config_factory->get(ConfigurationForm::SETTINGS);
  }

  /**
   * Setup the cache control.
   *
   * @param Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   Event object.
   */
  public function cacheControl(FilterResponseEvent $event) {
    $request = $event->getRequest();

    // If route has proxy object then change the response.
    $route_object = $request->get('_route_object');
    if ($route_object && $route_object->hasDefault('ajax_processor_path')) {
      $response = $event->getResponse();

      if ($response->isCacheable()) {
        $external_max_age = 300;
        if ($this->processorConfig) {
          $external_max_age = $this->processorConfig->get('max_age');
        }
        $response->setMaxAge($external_max_age);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    // Setting a low weight so that it is called at the end of cache control
    // customization by other modules.
    $events[KernelEvents::RESPONSE][] = ['cacheControl', -2048];
    return $events;
  }

}

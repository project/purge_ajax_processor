<?php

namespace Drupal\purge_ajax_processor\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\purge_ui\Form\ProcessorConfigFormBase;

/**
 * Configuration form for the Core Tags queuer.
 */
class ConfigurationForm extends ProcessorConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'purge_ajax_processor.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'purge_ajax_processor.configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['max_age'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum age'),
      '#default_value' => $config->get('max_age') ?? 300,
      '#description' => $this->t('Max-age for the purge ajax endpoint. Applicable for anonymous users.'),
      '#field_suffix' => $this->t('seconds'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    $this->config(static::SETTINGS)
      ->set('max_age', $form_state->getValue('max_age'))
      ->save();
  }

}

<?php

namespace Drupal\purge_ajax_processor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\purge\Plugin\Purge\Purger\Exception\CapacityException;
use Drupal\purge\Plugin\Purge\Purger\Exception\DiagnosticsException;
use Drupal\purge\Plugin\Purge\Purger\Exception\LockException;
use Drupal\purge_ajax_processor\Form\ConfigurationForm;

/**
 * A Class for AjaxProcessor.
 *
 * @package Drupal\purge_ajax_processor\Controller
 */
class AjaxProcessor extends ControllerBase {

  /**
   * The container object.
   *
   * @var Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * The processor plugin or FALSE when disabled.
   *
   * @var null|false|\Drupal\purge_ajax_processor\Plugin\Purge\Processor\AjaxProcessor
   */
  protected $processor = NULL;

  /**
   * The purge executive service, which wipes content from external caches.
   *
   * @var \Drupal\purge\Plugin\Purge\Purger\PurgersServiceInterface
   */
  protected $purgePurgers;

  /**
   * The queue in which to store, claim and release invalidation objects from.
   *
   * @var \Drupal\purge\Plugin\Purge\Queue\QueueServiceInterface
   */
  protected $purgeQueue;

  /**
   * Processor config.
   *
   * @var \Drupal\Core\Config\Config|null
   */
  protected $processorConfig = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(
      ContainerInterface $container
  ) {
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container
    );
  }

  /**
   * Initialize the services.
   *
   * @return bool
   *   TRUE when everything is available, FALSE when our plugin is disabled.
   */
  protected function initialize() {
    if (is_null($this->processor)) {
      // If the ajax processor plugin doesn't load, this object is not
      // allowed to operate and thus loads the least possible dependencies.
      $this->processor = $this->container->get('purge.processors')->get('ajax_processor');
      if ($this->processor !== FALSE) {
        $this->purgePurgers = $this->container->get('purge.purgers');
        $this->purgeQueue = $this->container->get('purge.queue');
        $this->processorConfig = $this->container->get('config.factory')->get(ConfigurationForm::SETTINGS);
      }
    }
    return $this->processor !== FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $this->executePurge();

    // Since AjaxResponse is not cacheable hence CacheableJsonResponse is used.
    // Return current timestamp as a cacheable json response.
    $response = new CacheableJsonResponse(
      time()
    );

    // Build the cachableMetadata.
    $cache_metadata = new CacheableMetadata();

    // Prepare external max-age.
    $external_max_age = 300;
    if ($this->processorConfig) {
      $external_max_age = $this->processorConfig->get('max_age');
      $response->addCacheableDependency($this->processorConfig);
    }

    // Make the response cacheable at drupal end for anonymous users.
    $drupal_max_age = 0;
    if ($this->currentUser()->isAnonymous()) {
      $drupal_max_age = $external_max_age;
    }

    $cache_metadata->mergeCacheMaxAge($drupal_max_age);
    $cache_metadata->addCacheContexts(['user.roles:anonymous']);

    // Set the max-age for cache control modules so that they don't interfere.
    $response->setMaxAge($external_max_age);
    if (function_exists('ape_cache_set')) {
      ape_cache_set($external_max_age);
    }

    // Attach cacheability data to response and return.
    $response->addCacheableDependency($cache_metadata);

    return $response;
  }

  /**
   * Execute the purge operation.
   */
  protected function executePurge() {
    if (!$this->initialize()) {
      return;
    }
    $claims = $this->purgeQueue->claim();
    try {
      $this->purgePurgers->invalidate($this->processor, $claims);
    }
    // Claim a chunk of invalidations, process and let the queue handle results.
    catch (DiagnosticsException $e) {
      // Diagnostic exceptions happen when the system cannot purge.
    }
    catch (CapacityException $e) {
      // Capacity exceptions happen when too much was purged this request.
    }
    catch (LockException $e) {
      // Lock exceptions happen when another code path is currently processing.
    }
    finally {
      $this->purgeQueue->handleResults($claims);
    }
  }

}

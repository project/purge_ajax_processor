(function($, Drupal, drupalSettings) {

  function trigger_purge() {
    const requestUrl = drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix + 'purge-ajax-processor';

    // Drupal.Ajax only supports post request hence $.Ajax has been used here.
    $.ajax({ url: requestUrl });
  }

  $(document).ready(function() {
    trigger_purge();
  });

})(jQuery, Drupal, drupalSettings);

